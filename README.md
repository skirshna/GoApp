## Go App
Purpose of this app is to learn GoLang.

We are given a input.txt that takes in transactions. We need to parse the input
file and validate every transaction. The result we generate need to match
output.txt.

The app validates and accepts the transaction based on the criteria: 
- daily transaction limit of $5000 max
- daily transaction count of 3 max
- weekly transaction limit of $20000 max
- Any duplicate transactions are eliminated.

### Code Structure
The codebase consist of three layers:
Main (also controller) -> Service -> Repository. 

- The repository adds the transaction from input.txt
file to the transactionChannel. 
- The service layer reads from the channel and checks if the 
transaction is valid based on the daily and weekly criteria and returns the transaction status.
- The main method converts the transaction status to json and prints it.
- The test transactionService_test validates the logic that approves or rejects transactions

### Dependencies
- "github.com/fxtlabs/date"
- "github.com/stretchr/testify/assert"

### To Run Locally
- Developed using GoLand
- Language: Go 1.15.6
- GOPATH = project root directory