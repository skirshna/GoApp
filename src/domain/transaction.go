package domain

import "time"

type Transaction struct {
	Id         int       `json:"id,string"`
	CustomerId int       `json:"customer_id,string"`
	LoadAmount string    `json:"load_amount"`
	Time       time.Time `json:"time"`
}

type TransactionStatus struct {
	Id         int       `json:"id,string"`
	CustomerId int       `json:"customer_id,string"`
	Accepted bool   	 	`json:"accepted"`
}

type DailyValidationFactors struct {
	RecentTransactionTime time.Time
	Amount float64
	Count int
}

type WeeklyTransactionValidation struct {
	RecentTransactionTime time.Time
	Amount float64
}
