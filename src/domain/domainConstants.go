package domain

const MaxAmtPerDay float64 = 5000
const MaxCountPerDay int = 3
const MaxAmtPerWeek float64 = 20000
