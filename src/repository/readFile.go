package repository

import (
	"bufio"
	"domain"
	"encoding/json"
	"io"
	"log"
	"os"
	"strings"
)

type Repository interface {
	ReadFile(transactionChannel chan <- domain.Transaction )
}

type FileRepository struct {
	Source string
}

func openFile(fileName string) *os.File {
	data, err := os.Open(fileName)
	if err != nil {
		log.Println("Opening file :" + fileName + "-"+err.Error())
		panic(err)
	}
	return data
}

func (f *FileRepository) ReadFile(transactionChannel chan <- domain.Transaction ) {
	var file = openFile(f.Source)
	defer file.Close()
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		var transaction domain.Transaction
		var line = scanner.Text()
		err := json.NewDecoder(strings.NewReader(line)).Decode(&transaction)
		if err == io.EOF {
			break
		} else if err != nil {log.Println("Unable to process the line from file:"+line)}
		transactionChannel <- transaction
	}
	defer close(transactionChannel)
}