package main

import (
	"domain"
	"fmt"
	"log"
	"repository"
	"service"
	"utilities"
)


func main() {
	var f=utilities.SetLogSource("logger.txt")
	log.SetOutput(f)
	defer f.Close()

	var rep = repository.FileRepository{
		Source: "src/repository/resources/input.txt",
	}
	var service = service.TransactionValidationService{
		DailyValidationFactorsByCustomers : make(map[int]domain.DailyValidationFactors),
		WeeklyValidationFactorsByCustomers : make(map[int]domain.WeeklyTransactionValidation),
		TransactionChannel : make(chan domain.Transaction),
		Repository: &rep,
	}

	var listOfTransactionStatus = service.ValidateTransactions()
	for _,status := range listOfTransactionStatus {
		fmt.Println(utilities.MapObjectToJsonString(status))
	}

}
