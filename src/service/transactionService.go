package service

import (
	"domain"
	"fmt"
	"repository"
)

type ITransactionValidationService interface {
	ValidateTransactions(rep repository.Repository)
}

type TransactionValidationService struct {
	DailyValidationFactorsByCustomers map[int]domain.DailyValidationFactors
	WeeklyValidationFactorsByCustomers map[int]domain.WeeklyTransactionValidation
	TransactionChannel chan domain.Transaction
	Repository repository.Repository
}

func (tvs *TransactionValidationService) ValidateTransactions() []domain.TransactionStatus {
	var list []domain.TransactionStatus
	go tvs.Repository.ReadFile(tvs.TransactionChannel)

	var SeenMap = make(map[string]bool)
	for t:= range tvs.TransactionChannel {
		tIdent := fmt.Sprintf("%d:%d", t.Id, t.CustomerId)
		if !SeenMap[tIdent] {
			list = append(list, tvs.ValidateOneTransaction(t))
			SeenMap[tIdent] = true
		}
	}

	return list
}

func (tvs *TransactionValidationService) ValidateOneTransaction(t domain.Transaction) domain.TransactionStatus {
	var isValid = ValidateTransactionsByDay(t, tvs.DailyValidationFactorsByCustomers) &&
		ValidateTransactionsByWeek(t, tvs.WeeklyValidationFactorsByCustomers)

	return tvs.CreateTransactionStatus(isValid, t)
}

func (tvs *TransactionValidationService) CreateTransactionStatus(status bool, t domain.Transaction) domain.TransactionStatus{
	var ts =  domain.TransactionStatus{
		CustomerId: t.CustomerId,
		Id: t.Id,
		Accepted: status,
	}

	return ts
}











