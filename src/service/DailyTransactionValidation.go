package service

import (
	"domain"
	"log"
	"utilities"
)

func ValidateTransactionsByDay(transaction domain.Transaction,
	factorsByCustomer map[int]domain.DailyValidationFactors) bool {
	isValid := false
	var validFactor = factorsByCustomer[transaction.CustomerId]

	if utilities.IsEqualDates(validFactor.RecentTransactionTime, transaction.Time)  {
		isValid =  processForCurrentDate(transaction, &validFactor)
	} else {
		isValid =  processForNewDate(transaction, &validFactor)
	}

	//}
	validFactor.RecentTransactionTime = transaction.Time
	factorsByCustomer[transaction.CustomerId] = validFactor

	return isValid
}

func processForCurrentDate(transaction domain.Transaction, factorsByCustomer *domain.DailyValidationFactors) bool {
	var transactionAmt, err = utilities.ConvertLoadAmountToFloat(transaction.LoadAmount)
	if err != nil {
		log.Printf("Skipping Transaction %d. Error: invalid LoadAmount %s", transaction.Id,
			transaction.LoadAmount)
		return false
	}
	if transactionAmt+factorsByCustomer.Amount > domain.MaxAmtPerDay || factorsByCustomer.Count >= domain.MaxCountPerDay {
		return false
	}

	factorsByCustomer.Amount += transactionAmt
	factorsByCustomer.Count +=1
	return true

}

func processForNewDate(transaction domain.Transaction, factorsByCustomer *domain.DailyValidationFactors) bool {
	var transactionAmt, err = utilities.ConvertLoadAmountToFloat(transaction.LoadAmount)
	if err != nil {
		log.Printf("Skipping Transaction %d. Error: invalid LoadAmount %s", transaction.Id,
			transaction.LoadAmount)
		return false
	}

	if transactionAmt > domain.MaxAmtPerDay {
		factorsByCustomer.Amount = 0
		factorsByCustomer.Count = 0
		return false
	}

	factorsByCustomer.Amount = transactionAmt
	factorsByCustomer.Count = 1
	return true
}
