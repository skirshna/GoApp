package service

import (
	"domain"
	"log"
	"utilities"
)

func ValidateTransactionsByWeek(transaction domain.Transaction, factorsByCustomer map[int]domain.WeeklyTransactionValidation) bool {
	isValid := false
	var validFactor = factorsByCustomer[transaction.CustomerId]
	if utilities.IsEqualWeeks(validFactor.RecentTransactionTime,transaction.Time) {
		isValid =  processForCurrentWeek(transaction, &validFactor)
	} else {
		isValid =  processForNewWeek(transaction, &validFactor)
	}


	validFactor.RecentTransactionTime = transaction.Time
	factorsByCustomer[transaction.CustomerId] = validFactor
	return isValid
}

func processForCurrentWeek(transaction domain.Transaction,
	factorsByCustomer *domain.WeeklyTransactionValidation) bool {
	var transactionAmt, err = utilities.ConvertLoadAmountToFloat(transaction.LoadAmount)
	if err != nil {
		log.Printf("Skipping Transaction %d. Error: invalid LoadAmount %s", transaction.Id,
			transaction.LoadAmount)
		return false
	}
	if transactionAmt+factorsByCustomer.Amount > domain.MaxAmtPerWeek  {
		return false
	}

	factorsByCustomer.Amount += transactionAmt
	return true
}

func processForNewWeek(transaction domain.Transaction,
	factorsByCustomer *domain.WeeklyTransactionValidation) bool {
	var transactionAmt, err = utilities.ConvertLoadAmountToFloat(transaction.LoadAmount)
	if err != nil {
		log.Printf("Skipping Transaction %d. Error: invalid LoadAmount %s", transaction.Id,
			transaction.LoadAmount)
		return false
	}

	if transactionAmt > domain.MaxAmtPerWeek  {
		factorsByCustomer.Amount = 0
		return false
	}

	factorsByCustomer.Amount = transactionAmt
	return true
}
