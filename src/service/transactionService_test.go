package service_test

import (
	"domain"
	"fmt"
	"github.com/stretchr/testify/assert"
	"service"
	"testing"
	"time"
)

func Test_GivenAListOfTransaction_CanValidateTheirStatus(t *testing.T) {
	var service = GenerateTestServiceStates()
	var expected = GetExpectedOutput()
	var listOfStates = service.ValidateTransactions()
	for i, ts := range listOfStates {
		assert.Equal(t,expected[i],ts.Accepted,"transaction status is not equal for Id "+ fmt.Sprint(ts.Id))
	}

}

func Test_GivenATransaction_WhenLoadAmtIsOverDailyLimit_RejectTransaction(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$10000", time.Now())
	var testService = GenerateTestService(testData, 4000, 2, 16000, time.Now())
	var expectedTestStatus = false
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
	assert.Equal(t, testData.CustomerId, ts.CustomerId, "They should be equal")
	assert.Equal(t, testData.Id, ts.Id, "They should be equal")
}

func Test_GivenATransaction_WhenLoadAmtSatisfyDailyLimit_AcceptTransaction(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$1000",time.Now())
	var testService = GenerateTestService(testData, 4000, 2, 16000, time.Now())
	var expectedTestStatus = true
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
}

func Test_GivenATransaction_WhenLoadAmtIsOverWeeklyLimit_RejectTransaction(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$5000",time.Now())
	var testService = GenerateTestService(testData, 4000, 2, 16000 , time.Now())
	var expectedTestStatus = false
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
}

func Test_GivenATransaction_WhenLoadAmtSatisfyWeeklyLimit_AcceptTransaction(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$3000",time.Now())
	var testService = GenerateTestService(testData, 1000, 2, 16000, time.Now())
	var expectedTestStatus = true
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
}

func Test_GivenATransaction_WhenCountIsOverDailyLimit_RejectTransaction(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$3000",time.Now())
	var testService = GenerateTestService(testData, 1000, 3, 16000, time.Now())
	var expectedTestStatus = false
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
}

func Test_GivenATransaction_WhenCountSatisfyDailyLimit_AcceptTransaction(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$3000",time.Now())
	var testService = GenerateTestService(testData, 1000, 2, 16000, time.Now())
	var expectedTestStatus = true
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
}

func Test_GivenATransaction_WhenDateChanges_ResetStates(t *testing.T) {
	var testData = GetTestTransaction(1,2,"$1500",time.Now())
	var testService = GenerateTestService(testData, 4000, 2, 16000,
		time.Now().AddDate(0,0,-2))
	var expectedTestStatus = true
	var ts = testService.ValidateOneTransaction(testData)
	assert.Equal(t, expectedTestStatus, ts.Accepted, "They should be equal")
}


func GenerateTestServiceStates() service.TransactionValidationService {
	var dailyMap = make(map[int]domain.DailyValidationFactors)
	var weeklyMap = make(map[int]domain.WeeklyTransactionValidation)
	var rep = TestRepo{}
	var testService = service.TransactionValidationService{
		DailyValidationFactorsByCustomers :dailyMap ,
		WeeklyValidationFactorsByCustomers : weeklyMap,
		TransactionChannel : make(chan domain.Transaction),
		Repository: &rep,
	}

	return testService
}

type TestRepo struct {}

func (t*TestRepo) populateChannel(tList []domain.Transaction, tc chan <- domain.Transaction)  {
	go func() {
		defer close(tc)
		for _, transaction := range tList {
			tc <- transaction
		}

	}()
}

func (t *TestRepo) ReadFile(tc chan <- domain.Transaction ) {
	var list = GetTransactionList()
	t.populateChannel(list,tc)
}

func GenerateTestService (testData domain.Transaction,
	dailyAmount float64, DailyCount int, weeklyAmt float64, time time.Time) service.TransactionValidationService {
	var mockDailyMap = make(map[int]domain.DailyValidationFactors)
	mockDailyMap[testData.CustomerId]=domain.DailyValidationFactors{
		Amount:                dailyAmount,
		Count:                 DailyCount,
		RecentTransactionTime: time,
	}

	var mockWeekMap = make(map[int]domain.WeeklyTransactionValidation)
	mockWeekMap[testData.CustomerId]=domain.WeeklyTransactionValidation{
		Amount: weeklyAmt,
		RecentTransactionTime: time,
	}

	var rep = TestRepo{}
	var testService = service.TransactionValidationService{
		DailyValidationFactorsByCustomers :mockDailyMap ,
		WeeklyValidationFactorsByCustomers : mockWeekMap,
		TransactionChannel : make(chan domain.Transaction),
		Repository: &rep,
	}

	return testService
}

func GetTestTransaction(id int, custId int, loadAmt string, time time.Time) domain.Transaction {
	var testData = domain.Transaction{
		Id: id,
		CustomerId: custId,
		LoadAmount: loadAmt,
		Time: time,
	}
	return testData
}

func GetTransactionList() []domain.Transaction {
	var now = time.Now()
	return []domain.Transaction {
		GetTestTransaction(1,2,"$5036",now),
		GetTestTransaction(2,2,"$3830", now.AddDate(0,0,1)),
		GetTestTransaction(3,2,"$5728", now.AddDate(0,0,2)),
		GetTestTransaction(4,2,"$3300", now.AddDate(0,0,2)),
	}
}

func GetExpectedOutput() []bool {
	return []bool {
		false, true, false, true,
	}
}

