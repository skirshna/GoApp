package utilities

import (
	"github.com/fxtlabs/date"
	"time"
)

func GetDate(time time.Time) date.Date {
	return date.NewAt(time)
}

func GetWeekByTime(time time.Time) (year, week int) {
	var tmp =  date.NewAt(time)
	return tmp.ISOWeek()
}

func IsEqualDates(t1 time.Time, t2 time.Time) bool {
	d1 := GetDate(t1)
	d2 := GetDate(t2)

	if d1 == d2 {
		return true
	}

	return false
}

func IsEqualWeeks(t1 time.Time, t2 time.Time) bool {
	w1,y1 := GetWeekByTime(t1)
	w2,y2 := GetWeekByTime(t2)

	if y1 == y2 && w1 == w2 {
		return true
	}

	return false
}