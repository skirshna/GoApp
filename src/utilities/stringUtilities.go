package utilities

import (
	"domain"
	"encoding/json"
	"log"
	"strconv"
)

func ConvertLoadAmountToFloat(loadAmount string) (float64, error) {
	return strconv.ParseFloat(loadAmount[1:],64)
}

//with generic: MapObjectToJsonString(type T)(T obj)
//not available: https://blog.golang.org/why-generics
func MapObjectToJsonString(obj domain.TransactionStatus) string {
	json, err := json.Marshal(obj)
	if err != nil {
		log.Println("Json Error for obj", obj)
		log.Println(err)
	}

	return string(json)
}