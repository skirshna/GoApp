package utilities

import (
	"log"
	"os"
)

func SetLogSource(filename string) *os.File {
	f, err := os.OpenFile(filename, os.O_RDWR | os.O_CREATE | os.O_APPEND, 0666)
	if err != nil {
		log.Printf("Error in opening file %v\n", err)
	}

	return f
}